import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '@/views/Home.vue';
import Book from '@/components/Book.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/book/:title',
    name: 'Book',
    component: Book,
    props: true
  }
];

const router = new VueRouter({
  mode: 'history',
  routes
});

export default router;
